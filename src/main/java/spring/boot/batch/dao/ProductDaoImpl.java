package spring.boot.batch.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.jdbc.core.JdbcTemplate;
import spring.boot.batch.entity.Producer;
import spring.boot.batch.entity.Product;
import spring.boot.batch.interfaces.IProductDao;
import spring.boot.batch.object_mapper.ProducerMapper;
import spring.boot.batch.object_mapper.ProductMapper;

import javax.sql.DataSource;
import java.util.List;

import static spring.boot.batch.sql.SqlQuery.*;

@Getter
@Setter
public class ProductDaoImpl implements IProductDao {

    private DataSource dataSource;

    @Override
    public void addProducer(Producer producer) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(
                UPDATE_PRODUCER,
                producer.getSeqNo(),
                producer.getProducerName(),
                producer.getSysStatus());
    }

    @Override
    public List<Producer> loadAllProducer() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query(PRODUCER_LOAD_ALL, new ProducerMapper());
    }

    @Override
    public List<Product> loadAllProduct() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query(PRODUCT_LOAD_ALL, new ProductMapper());
    }

}
