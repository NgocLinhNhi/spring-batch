package spring.boot.batch.inmem.bean;

import org.springframework.stereotype.Component;
import spring.boot.batch.constant.Constant;
import spring.boot.batch.inmem.common.BaseCache;
import spring.boot.batch.inmem.enums.LockingStrategy;
import spring.boot.batch.inmem.enums.StorageStrategy;
import spring.boot.batch.inmem.info.ProducerInfo;

@Component
public class ProducerRedisCache extends BaseCache<String, ProducerInfo> {
    public ProducerRedisCache(Class<ProducerInfo> type) {
        super(type);
    }

    public ProducerRedisCache() {
        super(ProducerInfo.class);
    }

    @Override
    protected String getId(ProducerInfo value) {
        return String.valueOf(value.getSeqNo());
    }

    @Override
    protected Double buildRedisKey(String entityKey) {
        return Double.longBitsToDouble(Long.parseLong(entityKey));
    }

    @Override
    protected String redisEntityKey() {
        return Constant.BATCH_PRODUCER;
    }

    @Override
    protected LockingStrategy getLockingStrategy() {
        return LockingStrategy.NONE;
    }

    @Override
    protected StorageStrategy getStorageStrategy() {
        return StorageStrategy.JACKSON2;
    }

    public void deleteAll() {
    }
}
