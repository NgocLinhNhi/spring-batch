package spring.boot.batch.inmem.cache;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public interface GenericCache<EK, HV extends Serializable> {

    boolean add(HV added);

    List<HV> findRange(EK from, EK to);

    long totalRecords();

    Long add(List<HV> addedSet);

    boolean deleteById(EK id) throws Exception;

    boolean delete(HV deleted) throws Exception;

    List<HV> findAll();

    Page<HV> findAll(Pageable pageable);

    Page<HV> findRange(EK from, EK to, Pageable pageable);

    HV findById(EK id) throws Exception;

    boolean update(HV updated, boolean retryIfFail) throws Exception;

    boolean saveOrUpdate(HV entity) throws Exception;

    boolean update(final Collection<EK> keys, final boolean retryIfFail, BiFunction<EK, HV, HV> updateCallback);

    boolean update(final Map<EK, Long> keyVersion, final boolean retryIfFail, BiFunction<EK, HV, HV> updateCallback);
}
