package spring.boot.batch.inmem.cache;

import java.io.Serializable;
import java.util.List;

public interface HashCache<K, V extends Serializable> {

    boolean save(V added);

    long totalRecords();

    boolean deleteById(K id) throws Exception;

    boolean delete(V deleted) throws Exception;

    void deleteAll();

    List<V> findAll();

    V findById(K id) throws Exception;

    boolean saveOrUpdate(V entity) throws Exception;
}
