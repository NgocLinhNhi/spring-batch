package spring.boot.batch.inmem.cache;

import java.io.Serializable;

public interface Versionable extends Serializable {
    long getVersion();

    void setVersion(long version);
}
