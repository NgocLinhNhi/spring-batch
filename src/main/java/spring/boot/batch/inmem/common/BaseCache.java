package spring.boot.batch.inmem.common;


import spring.boot.batch.inmem.impl.CrudGenericCacheImpl;

import java.io.Serializable;

public abstract class BaseCache<EK, HV extends Serializable> extends CrudGenericCacheImpl<EK, HV> {

    protected BaseCache(Class<HV> type) {
        super(type, RedisProvider.getInstance().getRedisCustomerTemplate());
    }


}
