package spring.boot.batch.inmem.common;

import spring.boot.batch.inmem.bean.ProducerRedisCache;
import spring.boot.batch.inmem.info.ProducerInfo;

import java.util.List;

public class RedisCache {
    private static ProducerRedisCache producerRedisCache = new ProducerRedisCache();

    public static void saveOrUpdateProducerCache(ProducerInfo value) {
        producerRedisCache.deleteAll();
        producerRedisCache.saveOrUpdate(value);
    }

    public static void removeProducerInfo(String producerName) {
        producerRedisCache.deleteById(producerName);
    }

    public static void saveOrUpdateProducerInfo(ProducerInfo producerInfo) {
        producerRedisCache.saveOrUpdate(producerInfo);
    }

    public static List<ProducerInfo> getAllProducerOnRedis() {
        return producerRedisCache.findAll();
    }
    

}
