package spring.boot.batch.inmem.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;

public class RedisProvider {

    private ApplicationContext appContext;
    private static final RedisProvider INSTANCE = new RedisProvider();

    private RedisProvider() {
        appContext = new ClassPathXmlApplicationContext(new String[]{"META-INF/spring-redis.xml"});
    }

    public static RedisProvider getInstance() {
        return INSTANCE;
    }

    public RedisTemplate getRedisTemplate() {
        RedisTemplate template = appContext.getBean("redisTemplate", RedisTemplate.class);
        return template;
    }

    public RedisTemplate getRedisCustomerTemplate() {
        RedisTemplate template = appContext.getBean("redisCustomerTemplate", RedisTemplate.class);
        return template;
    }
}
