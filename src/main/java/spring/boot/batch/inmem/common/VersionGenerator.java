package spring.boot.batch.inmem.common;

import java.lang.management.ManagementFactory;
import java.util.concurrent.atomic.AtomicLong;

public class VersionGenerator {

    private static final int APP_ID_BITS = 8;
    private static final int SEQUENCE_BITS = 15;
    private static final int TIMESTAMP_BITS = 40;
    private static final long APP_ID_MASK = (1L << APP_ID_BITS) - 1L;
    private static final long SEQUENCE_MASK = (1L << SEQUENCE_BITS) - 1L;
    private static final long TIMESTAMP_MASK = (1L << TIMESTAMP_BITS) - 1L;
    private static final long TIMESTAMP_OFFSET = 1420045200000L; //2015-01-01 00:00:00.000
    private static final AtomicLong sequence = new AtomicLong(1);

    public static VersionGenerator getDefault() {
        return INSTANCE;
    }

    private static final VersionGenerator INSTANCE = new VersionGenerator();

    private static final short PROCESS_ID = getProcessIdAsShort();

    public long next() {
        final long seq = VersionGenerator.sequence.getAndIncrement() & SEQUENCE_MASK;
        final long now = (System.currentTimeMillis() - TIMESTAMP_OFFSET) & TIMESTAMP_MASK;
        return (now << (SEQUENCE_BITS + APP_ID_BITS)) | (seq << APP_ID_BITS) | (PROCESS_ID & APP_ID_MASK);
    }

    private static short getProcessIdAsShort() {
        try {
            return Short.parseShort(getProcessId());
        } catch (Exception e) {
            return 0;
        }
    }

    private static String getProcessId() {
        return ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
    }
}
