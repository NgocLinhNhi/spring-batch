package spring.boot.batch.inmem.enums;


import lombok.Getter;

public enum ExpirationStrategy {
    FROM_CREATION(0), LAST_ACCESS(1);

    @Getter
    private int value;

    ExpirationStrategy(int value) {
        this.value = value;
    }
}
