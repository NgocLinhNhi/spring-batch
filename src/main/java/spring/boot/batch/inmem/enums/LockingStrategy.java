package spring.boot.batch.inmem.enums;

import lombok.Getter;

public enum LockingStrategy {
    NONE(0), TABLE_LEVEL(1), ROW_LEVEL(2);

    @Getter
    private int value;

    LockingStrategy(int value) {
        this.value = value;
    }
}
