package spring.boot.batch.inmem.enums;

import lombok.Getter;

public enum StorageStrategy {
    JDK(0), JACKSON2(2);

    @Getter
    private int value;

    StorageStrategy(int value) {
        this.value = value;
    }
}
