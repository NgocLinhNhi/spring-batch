package spring.boot.batch.inmem.impl;

import com.google.common.base.Function;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import spring.boot.batch.inmem.cache.GenericCache;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

public abstract class CrudGenericCacheImpl<EK, HV extends Serializable> extends GenericCacheBase<EK, HV> implements GenericCache<EK, HV> {

    private final Function<String, String> redisEntityKeyFunction;

    protected CrudGenericCacheImpl(Class<HV> type) {
        super(type);
        redisEntityKeyFunction = new Function<String, String>() {
            @Override
            public String apply(String entityKey) {
                return entityKey;
            }
        };
    }

    protected CrudGenericCacheImpl(Class<HV> type, RedisTemplate<String, HV> redisTemplate) {
        super(type, redisTemplate);
        redisEntityKeyFunction = new Function<String, String>() {
            @Override
            public String apply(String entityKey) {
                return entityKey;
            }
        };
    }

    @Override
    public boolean add(HV added) {
        return super.add(added, redisEntityKeyFunction);
    }

    @Override
    public boolean deleteById(EK id) {
        return super.deleteById(id, redisEntityKeyFunction);
    }

    @Override
    public List<HV> findAll() {
        return super.findAll(redisEntityKeyFunction);
    }

    @Override
    public List<HV> findRange(EK from, EK to) {
        return super.findRange(from, to, redisEntityKeyFunction);
    }

    @Override
    public HV findById(EK id) {
        return super.findById(id, redisEntityKeyFunction);
    }

    @Override
    public long totalRecords() {
        return super.totalRecords(redisEntityKeyFunction);
    }

    @Override
    public Long add(final List<HV> addedSet) {
        return super.add(addedSet, redisEntityKeyFunction);
    }

    @Override
    public boolean delete(HV deleted) throws Exception {
        return super.delete(deleted, redisEntityKeyFunction);
    }

    @Override
    public void deleteKey(String key) throws Exception {
        super.deleteKey(key);
    }

    public Set<EK> getKeys(String pattern) throws Exception {
        return super.getKeys(pattern);
    }

    @Override
    public Page<HV> findAll(Pageable pageable) {
        return super.findAll(pageable, redisEntityKeyFunction);
    }

    @Override
    public Page<HV> findRange(EK from, EK to, Pageable pageable) {
        return super.findRange(from, to, pageable, redisEntityKeyFunction);
    }

    @Override
    public boolean update(final HV updated, final boolean retryIfFail) {
        return super.update(updated, retryIfFail, redisEntityKeyFunction);
    }

    @Override
    public boolean saveOrUpdate(HV entity) {
        return super.saveOrUpdate(entity, redisEntityKeyFunction);
    }

    @Override
    public boolean update(final Collection<EK> keys, final boolean retryIfFail, BiFunction<EK, HV, HV> updateCallback) {
        return super.update(keys, retryIfFail, updateCallback, redisEntityKeyFunction);
    }

    @Override
    public boolean update(Map<EK, Long> keys, boolean retryIfFail, BiFunction<EK, HV, HV> updateCallback) {
        return super.update(keys, retryIfFail, updateCallback, redisEntityKeyFunction);
    }
}
