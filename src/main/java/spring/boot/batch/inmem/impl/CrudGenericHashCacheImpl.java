package spring.boot.batch.inmem.impl;

import com.google.common.base.Function;
import spring.boot.batch.inmem.cache.HashCache;

import java.io.Serializable;

public abstract class CrudGenericHashCacheImpl<K, V extends Serializable> extends GenericHashCacheBase<K, V> implements HashCache<K, V> {
    private final Function<String, String> redisEntityKeyFunction = new Function<String, String>() {
        public String apply(String entityKey) {
            return entityKey;
        }
    };

    protected CrudGenericHashCacheImpl(Class<V> type) {
        super(type);
    }

    public boolean save(V value) {
        return super.save(value, this.redisEntityKeyFunction);
    }

    public boolean deleteById(K id) {
        return super.deleteById(id, this.redisEntityKeyFunction);
    }


    public long totalRecords() {
        return super.totalRecords(this.redisEntityKeyFunction);
    }

    public boolean delete(V deleted) throws Exception {
        return super.delete(deleted, this.redisEntityKeyFunction);
    }

    public boolean saveOrUpdate(V entity) throws Exception {
        return super.saveOrUpdate(entity, this.redisEntityKeyFunction);
    }

    public V findById(K id) {
        return super.findById(id, this.redisEntityKeyFunction);
    }

}
