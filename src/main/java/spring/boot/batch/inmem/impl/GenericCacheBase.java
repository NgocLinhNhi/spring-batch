package spring.boot.batch.inmem.impl;

import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import spring.boot.batch.inmem.cache.Versionable;
import spring.boot.batch.inmem.common.RedisProvider;
import spring.boot.batch.inmem.common.VersionGenerator;
import spring.boot.batch.inmem.enums.LockingStrategy;
import spring.boot.batch.inmem.enums.StorageStrategy;

import java.io.Serializable;
import java.util.*;
import java.util.function.BiFunction;

public abstract class GenericCacheBase<EK, HV extends Serializable> {

    protected final Class<HV> type;
    protected final LockingStrategy lockingStrategy;
    protected final StorageStrategy storageStrategy;
    protected final RedisTemplate<String, HV> redisTemplate;
    private static Logger logger = LoggerFactory.getLogger(GenericCacheBase.class);

    public static final int DELAY = 1;
    public static final int RETRY_ATTEMPTS = 15;

    protected GenericCacheBase(Class<HV> type) {
        this.type = type;
        this.redisTemplate = RedisProvider.getInstance().getRedisTemplate();
        this.lockingStrategy = getLockingStrategy() == null ? LockingStrategy.NONE : getLockingStrategy();
        this.storageStrategy = getStorageStrategy() == null ? StorageStrategy.JDK : getStorageStrategy();
        RedisSerializer<?> defaultSerializer;
        switch (this.storageStrategy) {
            case JACKSON2:
                defaultSerializer = new Jackson2JsonRedisSerializer<HV>(this.type);
                break;
            default:
                defaultSerializer = new JdkSerializationRedisSerializer();
                break;
        }
        this.redisTemplate.setValueSerializer(defaultSerializer);
    }

    protected GenericCacheBase(Class<HV> type, RedisTemplate<String, HV> redisTemplate) {
        this.type = type;
        this.redisTemplate = redisTemplate;
        this.lockingStrategy = getLockingStrategy() == null ? LockingStrategy.NONE : getLockingStrategy();
        this.storageStrategy = getStorageStrategy() == null ? StorageStrategy.JDK : getStorageStrategy();
        RedisSerializer<?> defaultSerializer;
        switch (this.storageStrategy) {
            case JACKSON2:
                defaultSerializer = new Jackson2JsonRedisSerializer<HV>(this.type);
                break;
            default:
                defaultSerializer = new JdkSerializationRedisSerializer();
                break;
        }
        this.redisTemplate.setValueSerializer(defaultSerializer);
    }

    protected abstract String redisEntityKey();

    protected abstract Double buildRedisKey(EK entityKey);

    protected abstract EK getId(HV value);

    protected abstract LockingStrategy getLockingStrategy();

    protected abstract StorageStrategy getStorageStrategy();

    protected String redisEntityLockPrefix() {
        return type.getSimpleName().toUpperCase() + ":LOCK";
    }

    protected String buildLockKey(EK id) {
        return String.format("%s:%s", redisEntityLockPrefix(), id);
    }

    protected boolean add(HV added, Function<String, String> redisEntityKeyFunction) {
        logger.debug("Adding {} with information: {}", redisEntityKeyFunction.apply(redisEntityKey()), added);

        Double key = buildRedisKey(getId(added));
        boolean result = true;
        if (LockingStrategy.ROW_LEVEL.equals(lockingStrategy)) {
            RedisTemplate<String, Long> redisLockTemplate = (RedisTemplate<String, Long>) redisTemplate;
            result = redisLockTemplate.opsForValue().setIfAbsent(buildLockKey(getId(added)), System.nanoTime());
        }
        result = result ? redisTemplate.opsForZSet().add(redisEntityKeyFunction.apply(redisEntityKey()), added, key) : result;

        logger.debug("{} to adding {} with information: {}", result ? "Success" : "Fail", redisEntityKeyFunction.apply(redisEntityKey()), added);
        return result;
    }

    protected boolean deleteById(EK id, Function<String, String> redisEntityKeyFunction) {
        Double key = buildRedisKey(id);
        logger.debug("Delete {} by id: {}", redisEntityKeyFunction.apply(redisEntityKey()), id);

        if (LockingStrategy.ROW_LEVEL.equals(lockingStrategy)) {
            redisTemplate.delete(buildLockKey(id));
        }

        Long result = redisTemplate.opsForZSet().removeRangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), key, key);

        return result > 0;
    }

    protected List<HV> findAll(Function<String, String> redisEntityKeyFunction) {
        logger.debug("Finding all: {}", redisEntityKeyFunction.apply(redisEntityKey()));
        Set<HV> result = redisTemplate.opsForZSet().range(redisEntityKeyFunction.apply(redisEntityKey()), 0, totalRecords(redisEntityKeyFunction));
        logger.debug("Returning {} {}", result.size(), redisEntityKeyFunction.apply(redisEntityKey()));
        return new ArrayList<HV>(result);
    }

    protected List<HV> findRange(EK from, EK to, Function<String, String> redisEntityKeyFunction) {
        logger.debug("Finding range {} from {} to {}", redisEntityKeyFunction.apply(redisEntityKey()), from, to);
        Set<HV> result = redisTemplate.opsForZSet().rangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), buildRedisKey(from), buildRedisKey(to));
        logger.debug("Returning {} {}", result.size(), redisEntityKeyFunction.apply(redisEntityKey()));
        return new ArrayList<HV>(result);
    }

    protected HV findById(EK id, Function<String, String> redisEntityKeyFunction) {
        Double key = buildRedisKey(id);
        logger.debug("Finding {} by id: {}", redisEntityKeyFunction.apply(redisEntityKey()), id);
        return fetch(redisTemplate.opsForZSet(), key, redisEntityKeyFunction);
    }

    protected long totalRecords(Function<String, String> redisEntityKeyFunction) {
        return redisTemplate.opsForZSet().size(redisEntityKeyFunction.apply(redisEntityKey()));
    }

    protected Long add(final List<HV> addedSet, Function<String, String> redisEntityKeyFunction) {
        logger.debug("Adding {} {} to Cache", addedSet.size(), redisEntityKeyFunction.apply(redisEntityKey()));
        Set<ZSetOperations.TypedTuple<HV>> tuples = new HashSet<ZSetOperations.TypedTuple<HV>>();
        Map<String, Long> lockMap = new HashMap<String, Long>();
        boolean insertLock = true;
        for (HV added : addedSet) {
            tuples.add(new DefaultTypedTuple<HV>(added, Double.valueOf(buildRedisKey(getId(added)))));
            if (LockingStrategy.ROW_LEVEL.equals(lockingStrategy)) {
                lockMap.put(buildLockKey(getId(added)), System.nanoTime());
            }
        }
        RedisTemplate<String, Long> redisLockTemplate = (RedisTemplate<String, Long>) redisTemplate;
        if (LockingStrategy.ROW_LEVEL.equals(lockingStrategy)) {
            insertLock = redisLockTemplate.opsForValue().multiSetIfAbsent(lockMap);
        }
        Long result = insertLock ? redisTemplate.opsForZSet().add(redisEntityKeyFunction.apply(redisEntityKey()), tuples) : 0;
        logger.debug("Added {} {} to Cache", result, redisEntityKeyFunction.apply(redisEntityKey()));
        return result;
    }

    protected boolean delete(HV deleted, Function<String, String> redisEntityKeyFunction) {
        return deleteById(getId(deleted), redisEntityKeyFunction);
    }

    protected void deleteKey(String key) throws Exception {
        redisTemplate.delete(key);
    }

    protected Set<EK> getKeys(String pattern) throws Exception {
        return (Set<EK>) redisTemplate.keys(pattern);
    }

    protected Page<HV> findAll(Pageable pageable, Function<String, String> redisEntityKeyFunction) {
        logger.debug("Finding all {} with pageable {}", redisEntityKeyFunction.apply(redisEntityKey()), pageable);
        long totalRecords = totalRecords(redisEntityKeyFunction);
        Set<HV> entities = redisTemplate.opsForZSet().rangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), 0l, totalRecords, pageable.getOffset(), pageable.getPageSize());
        logger.debug("Returning {} {} with total records {}", entities.size(), redisEntityKeyFunction.apply(redisEntityKey()), totalRecords);
        return new PageImpl<HV>(new ArrayList<HV>(entities), pageable, totalRecords);
    }

    protected Page<HV> findRange(EK from, EK to, Pageable pageable, Function<String, String> redisEntityKeyFunction) {
        logger.debug("Finding range {} from {} to {}", redisEntityKeyFunction.apply(redisEntityKey()), from, to);
        Double fromKey = buildRedisKey(from);
        Double toKey = buildRedisKey(to);
        long totalRecords = redisTemplate.opsForZSet().count(redisEntityKeyFunction.apply(redisEntityKey()), fromKey, toKey);
        Set<HV> entities = redisTemplate.opsForZSet().rangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), fromKey, toKey, pageable.getOffset(), pageable.getPageSize());
        logger.debug("Returning {} {} with total records {}", entities.size(), redisEntityKeyFunction.apply(redisEntityKey()), totalRecords);
        return new PageImpl<HV>(new ArrayList<HV>(entities), pageable, totalRecords);
    }

    protected boolean update(final HV updated, final boolean retryIfFail, Function<String, String> redisEntityKeyFunction) {
        logger.debug("Updating {} with information: {}", redisEntityKeyFunction.apply(redisEntityKey()), updated);
        if (updated instanceof Versionable) {
            ((Versionable) updated).setVersion(VersionGenerator.getDefault().next());
        }
        boolean retVal = update(Arrays.asList(getId(updated)), null, retryIfFail, new BiFunction<ZSetOperations<String, HV>, Double, HV>() {
            @Override
            public HV apply(ZSetOperations<String, HV> stringHVZSetOperations, Double aDouble) {
                return updated;
            }
        }, new BiFunction<EK, HV, HV>() {
            @Override
            public HV apply(EK ek, HV hv) {
                return hv;
            }
        }, redisEntityKeyFunction);
        logger.debug("Updated {} with information: {}", redisEntityKeyFunction.apply(redisEntityKey()), getId(updated));
        return retVal;
    }

    protected boolean update(final Collection<EK> keys, final boolean retryIfFail, BiFunction<EK, HV, HV> updateCallback, final Function<String, String> redisEntityKeyFunction) {
        return update(keys, null, retryIfFail, new BiFunction<ZSetOperations<String, HV>, Double, HV>() {
            @Override
            public HV apply(ZSetOperations<String, HV> zSetOperations, Double aDouble) {
                return fetch(zSetOperations, aDouble, redisEntityKeyFunction);
            }
        }, updateCallback, redisEntityKeyFunction);
    }

    protected boolean update(final Map<EK, Long> keyVersion, final boolean retryIfFail, BiFunction<EK, HV, HV> updateCallback, final Function<String, String> redisEntityKeyFunction) {
        return update(keyVersion.keySet(), keyVersion, retryIfFail, new BiFunction<ZSetOperations<String, HV>, Double, HV>() {
            @Override
            public HV apply(ZSetOperations<String, HV> zSetOperations, Double aDouble) {
                return fetch(zSetOperations, aDouble, redisEntityKeyFunction);
            }
        }, updateCallback, redisEntityKeyFunction);
    }

    protected boolean saveOrUpdate(HV entity, Function<String, String> redisEntityKeyFunction) {
        Double key = buildRedisKey(getId(entity));
        Set<HV> result = redisTemplate.opsForZSet().rangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), key, key);
        if (result == null || result.size() == 0) {
            return add(entity, redisEntityKeyFunction);
        } else {
            return update(entity, true, redisEntityKeyFunction);
        }
    }

    private boolean update(final Collection<EK> keys, final Map<EK, Long> keyVersion, final boolean retryIfFail, final BiFunction<ZSetOperations<String, HV>, Double, HV> fetchCallback, final BiFunction<EK, HV, HV> updateCallback, final Function<String, String> redisEntityKeyFunction) {
        boolean retVal = true;
        if (updateCallback == null || fetchCallback == null) {
            throw new UnsupportedOperationException("callback is required for this operation");
        } else {
            if (LockingStrategy.NONE.equals(lockingStrategy)) {
                ZSetOperations<String, HV> zSetOperations = redisTemplate.opsForZSet();
                Map<Double, HV> entityToUpdate = new HashMap<Double, HV>(keys.size());
                for (EK key : keys) {
                    Double entityKey = buildRedisKey(key);
                    HV entity = fetchCallback.apply(zSetOperations, entityKey);
                    if (entity instanceof Versionable && keyVersion != null && keyVersion.get(key) != null && keyVersion.get(key) != ((Versionable) entity).getVersion()) {
                        return false;
                    }
                    entity = updateCallback.apply(key, entity);
                    if (entity instanceof Versionable) {
                        ((Versionable) entity).setVersion(VersionGenerator.getDefault().next());
                    }
                    entityToUpdate.put(entityKey, entity);
                }
                for (Map.Entry<Double, HV> entry : entityToUpdate.entrySet()) {
                    zSetOperations.removeRangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), entry.getKey(), entry.getKey());
                    zSetOperations.add(redisEntityKeyFunction.apply(redisEntityKey()), entry.getValue(), entry.getKey());
                }
            } else {
                final List<Object> finalResult = redisTemplate.execute(new SessionCallback<List<Object>>() {
                    public List<Object> execute(RedisOperations operations) throws DataAccessException {
                        List<Object> results = new ArrayList<>();
                        int i = 0;
                        try {
                            while (results.isEmpty() && i < (retryIfFail ? RETRY_ATTEMPTS : 1)) {
                                try {
                                    if (i > 0) {
                                        Thread.sleep(DELAY << i);
                                    }
                                } catch (InterruptedException e) {
                                    new RuntimeException(e);
                                }
                                ZSetOperations zSetOperations = operations.opsForZSet();
                                Map<EK, String> rowLockKeyMap = new HashMap<EK, String>();
                                Map<Map.Entry<EK, Double>, HV> fetchEntities = redisWatch(operations, zSetOperations, rowLockKeyMap, keys, fetchCallback, redisEntityKeyFunction);
                                for (HV item : fetchEntities.values()) {
                                    EK key = getId(item);
                                    if (item instanceof Versionable && keyVersion != null && keyVersion.get(key) != null && keyVersion.get(key) != ((Versionable) item).getVersion()) {
//                                        return null;
                                        logger.info("Version mismatch , try again");
                                        continue;
                                    } else if (item instanceof Versionable) {
                                        ((Versionable) item).setVersion(VersionGenerator.getDefault().next());
                                    }
                                }
                                operations.multi();
                                redisMulti(operations.opsForValue(), zSetOperations, rowLockKeyMap, fetchEntities, updateCallback, redisEntityKeyFunction);
                                // This will contain the results of all ops in the transaction
                                List<Object> execResult = operations.exec();
                                if (execResult != null)
                                    results.addAll(execResult);
                                i++;
                            }
                        } finally {
                            operations.unwatch();
                        }
                        return results;
                    }
                });
                retVal = finalResult != null;
            }
            return retVal;
        }
    }

    protected Map<Map.Entry<EK, Double>, HV> redisWatch(RedisOperations operations, ZSetOperations<String, HV> zSetOperations, Map<EK, String> rowLockKeyMap, Collection<EK> keys, BiFunction<ZSetOperations<String, HV>, Double, HV> fetchCallback, Function<String, String> redisEntityKeyFunction) {
        for (EK key : keys) {
            rowLockKeyMap.put(key, buildLockKey(key));
        }
        if (LockingStrategy.TABLE_LEVEL.equals(lockingStrategy)) {
            operations.watch(redisEntityKeyFunction.apply(redisEntityKey()));
        } else {
            operations.watch(rowLockKeyMap.values());
        }
        Map<Map.Entry<EK, Double>, HV> fetchEntities = new HashMap<Map.Entry<EK, Double>, HV>(keys.size());
        for (EK key : keys) {
            Double entityKey = buildRedisKey(key);
            fetchEntities.put(new AbstractMap.SimpleEntry<EK, Double>(key, entityKey), fetchCallback.apply(zSetOperations, entityKey));
        }
        return fetchEntities;
    }

    protected void redisMulti(ValueOperations operations, ZSetOperations zSetOperations, Map<EK, String> rowLockKeyMap, Map<Map.Entry<EK, Double>, HV> fetchEntities, BiFunction<EK, HV, HV> updateCallback, Function<String, String> redisEntityKeyFunction) {
        for (Map.Entry<Map.Entry<EK, Double>, HV> entry : fetchEntities.entrySet()) {
            if (LockingStrategy.ROW_LEVEL.equals(lockingStrategy)) {
                operations.set(rowLockKeyMap.get(entry.getKey().getKey()), System.nanoTime());
            }
            Double entityKey = entry.getKey().getValue();
            EK id = entry.getKey().getKey();
            HV updated = updateCallback.apply(id, entry.getValue());
            zSetOperations.removeRangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), entityKey, entityKey);
            zSetOperations.add(redisEntityKeyFunction.apply(redisEntityKey()), updated, entityKey);
        }
    }

    private HV fetch(ZSetOperations<String, HV> zSetOperations, Double key, Function<String, String> redisEntityKeyFunction) {
        Set<HV> result = zSetOperations.rangeByScore(redisEntityKeyFunction.apply(redisEntityKey()), key, key);
        Iterator<HV> iterator = result.iterator();
        if (iterator.hasNext()) {
            return iterator.next();
        }
        return null;
    }
}
