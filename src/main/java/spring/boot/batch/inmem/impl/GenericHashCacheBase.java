package spring.boot.batch.inmem.impl;

import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import spring.boot.batch.inmem.common.RedisProvider;
import spring.boot.batch.inmem.enums.LockingStrategy;
import spring.boot.batch.inmem.enums.StorageStrategy;

import java.io.Serializable;

public abstract class GenericHashCacheBase<K, V extends Serializable> {

    private final Class<V> type;
    private final LockingStrategy lockingStrategy;
    private final StorageStrategy storageStrategy;
    protected final RedisTemplate<String, V> redisTemplate;
    private static Logger logger = LoggerFactory.getLogger(GenericHashCacheBase.class);

    protected GenericHashCacheBase(Class<V> type) {
        this.type = type;
        this.redisTemplate = RedisProvider.getInstance().getRedisTemplate();
        this.lockingStrategy = getLockingStrategy() == null ? LockingStrategy.NONE : getLockingStrategy();
        this.storageStrategy = getStorageStrategy() == null ? StorageStrategy.JDK : getStorageStrategy();
        RedisSerializer<?> defaultSerializer;
        switch (this.storageStrategy) {
            case JACKSON2:
                defaultSerializer = new Jackson2JsonRedisSerializer<V>(this.type);
                break;
            default:
                defaultSerializer = new JdkSerializationRedisSerializer();
                break;
        }
        this.redisTemplate.setValueSerializer(defaultSerializer);
    }

    protected abstract K getId(V value);

    protected abstract String redisEntityKey();

    protected abstract String buildRedisKey(K entityKey);

    protected abstract LockingStrategy getLockingStrategy();

    protected abstract StorageStrategy getStorageStrategy();

    protected String redisEntityLockPrefix() {
        return type.getSimpleName().toUpperCase() + ":LOCK";
    }

    protected String buildLockKey(K id) {
        return String.format("%s:%s", redisEntityLockPrefix(), id);
    }

    protected boolean save(V added, Function<String, String> redisEntityKeyFunction) {
        logger.debug("Adding {} with information: {}", redisEntityKeyFunction.apply(redisEntityKey()), added);

        String key = buildRedisKey(getId(added));
        boolean result = true;
        if (LockingStrategy.ROW_LEVEL.equals(lockingStrategy)) {
            RedisTemplate<String, Long> redisLockTemplate = (RedisTemplate<String, Long>) redisTemplate;
            result = redisLockTemplate.opsForValue().setIfAbsent(buildLockKey(getId(added)), System.nanoTime());
        }
        if (result)
            redisTemplate.opsForHash().put(redisEntityKeyFunction.apply(redisEntityKey()), key, added);

        logger.debug("{} to adding {} with information: {}", result ? "Success" : "Fail", redisEntityKeyFunction.apply(redisEntityKey()), added);
        return result;
    }

    protected boolean saveOrUpdate(V entity, Function<String, String> redisEntityKeyFunction) {
        String key = buildRedisKey(getId(entity));
        V result = (V) redisTemplate.opsForHash().get(redisEntityKeyFunction.apply(redisEntityKey()), key);
        if (result == null) {
            return save(entity, redisEntityKeyFunction);
        } else {
            boolean deleted = deleteById(getId(entity), redisEntityKeyFunction);
            if (deleted)
                return save(entity, redisEntityKeyFunction);
            return deleted;
        }
    }

    protected boolean delete(V deleted, Function<String, String> redisEntityKeyFunction) {
        return deleteById(getId(deleted), redisEntityKeyFunction);
    }

    protected boolean deleteById(K id, Function<String, String> redisEntityKeyFunction) {
        String key = buildRedisKey(id);
        logger.debug("Delete {} by id: {}", redisEntityKeyFunction.apply(redisEntityKey()), id);

        if (LockingStrategy.ROW_LEVEL.equals(lockingStrategy)) {
            redisTemplate.delete(buildLockKey(id));
        }

        redisTemplate.opsForHash().delete(redisEntityKeyFunction.apply(redisEntityKey()), key);

        return true;
    }

    protected V findById(K id, Function<String, String> redisEntityKeyFunction) {
        String key = buildRedisKey(id);
        logger.debug("Finding {} by id: {}", redisEntityKeyFunction.apply(redisEntityKey()), id);
        return (V) redisTemplate.opsForHash().get(redisEntityKeyFunction.apply(redisEntityKey()), key);
    }

    protected long totalRecords(Function<String, String> redisEntityKeyFunction) {
        return redisTemplate.opsForHash().size(redisEntityKeyFunction.apply(redisEntityKey()));
    }
}
