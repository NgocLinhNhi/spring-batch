package spring.boot.batch.inmem.info;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProducerInfo implements Serializable {
    private int seqNo;
    private String producerName;
    private int sysStatus;
}
