package spring.boot.batch.interfaces;

import spring.boot.batch.entity.Producer;
import spring.boot.batch.entity.Product;

import java.util.List;

public interface IProductDao {
    void addProducer(Producer producer);

    List<Producer> loadAllProducer();

    List<Product> loadAllProduct();
}
