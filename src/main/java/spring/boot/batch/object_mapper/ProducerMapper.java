package spring.boot.batch.object_mapper;

import org.springframework.jdbc.core.RowMapper;
import spring.boot.batch.entity.Producer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProducerMapper implements RowMapper<Producer> {

	@Override
	public Producer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Producer pro = new Producer();
		pro.setSeqNo(rs.getInt("seq_no"));
		pro.setProducerName(rs.getString("producer_name"));
		pro.setSysStatus(rs.getInt("sys_status"));
		return pro;
	}

}