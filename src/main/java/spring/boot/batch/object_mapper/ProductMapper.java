package spring.boot.batch.object_mapper;

import org.springframework.jdbc.core.RowMapper;
import spring.boot.batch.entity.Product;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product pro = new Product();
        pro.setSeqPro(rs.getLong("seq_pro"));
        pro.setProductName(rs.getString("product_Name"));
        pro.setPrice(rs.getBigDecimal("price"));
        return pro;
    }

}