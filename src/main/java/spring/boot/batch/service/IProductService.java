package spring.boot.batch.service;

import spring.boot.batch.entity.Producer;

import java.util.List;

public interface IProductService {

    List<Producer> getListProducer();

    void saveProducerOnRedis(List<Producer> listProducer);

    void getListProducerCache();

    void exportToFile(List<Producer> producers, String reportDate, String absolutePath);
}
