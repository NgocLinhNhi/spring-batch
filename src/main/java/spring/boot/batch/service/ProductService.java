package spring.boot.batch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spring.boot.batch.dao.ProductDaoImpl;
import spring.boot.batch.entity.Producer;
import spring.boot.batch.inmem.bean.ProducerRedisCache;
import spring.boot.batch.inmem.common.RedisCache;
import spring.boot.batch.inmem.info.ProducerInfo;
import spring.boot.batch.tasklet.ProducerTasklet;
import spring.boot.batch.utils.csv.SimpleCsvPrinter;

import java.io.File;
import java.util.List;

import static spring.boot.batch.constant.Constant.*;

public class ProductService implements IProductService {

    private static final String CSV_EXPORT_FILE_NAME = "producer.csv";
    private static final String CSV_SEPARATE = ",";

    private final Logger logger = LoggerFactory.getLogger(ProducerTasklet.class);

    private ProductDaoImpl productDao;
    private ProducerRedisCache producerRedisCache;

    public ProductService(ProductDaoImpl productDao, ProducerRedisCache producerRedisCache) {
        this.productDao = productDao;
        this.producerRedisCache = producerRedisCache;
    }

    @Override
    public List<Producer> getListProducer() {
        logger.info("START Load Producers");
        List<Producer> producers = productDao.loadAllProducer();
        producers.forEach(producer -> System.out.println(producer.getProducerName()));
        logger.info("END Load Producers");
        return producers;
    }

    //Save to redis
    @Override
    public void saveProducerOnRedis(List<Producer> listProducer) {
        logger.info("START update data for redis =================== ");
        updateRedisCache(listProducer);
        logger.info("END update data for redis =================== ");
    }

    //load data from Redis
    @Override
    public void getListProducerCache() {
        logger.info("START get data from redis =================== ");
        //C1: Lấy từ RedisCache
        //List<ProducerInfo> listCache = RedisCache.getAllProducerOnRedis();
        //C2: Lấy từ producerRedisCache
        List<ProducerInfo> listCache = producerRedisCache.findAll();
        listCache.forEach(producerInfo -> System.out.println("Data from redis: " + producerInfo.getProducerName()));
        logger.info("END get data from redis =================== ");
    }

    @Override
    public void exportToFile(List<Producer> producers,
                             String reportDate,
                             String absolutePath) {
        logger.info("Start Export Producer to csv file ");
        exportCsvFile(producers, reportDate, absolutePath);
        logger.info("End Export Producer to csv file ");
    }

    private void exportCsvFile(List<Producer> producers,
                               String reportDate,
                               String absolutePath) {
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter(getCsvFileName(absolutePath))) {
            printer.write(buildHeader().toString());
            for (Producer producer : producers) {
                printer.write(
                        reportDate,
                        String.valueOf(producer.getSeqNo()),
                        producer.getProducerName(),
                        producer.getSysStatus() == 1 ? ACTIVE : UN_ACTIVE
                );
            }
            printer.flush();
            producers.clear();
        }
    }

    private StringBuilder buildHeader() {
        StringBuilder buildHeader = new StringBuilder();

        buildHeader.append(DATE);
        buildHeader.append(CSV_SEPARATE);
        buildHeader.append(SEQ);
        buildHeader.append(CSV_SEPARATE);
        buildHeader.append(PRODUCER_NAME);
        buildHeader.append(CSV_SEPARATE);
        buildHeader.append(STATUS);

        return buildHeader;
    }

    private void updateRedisCache(List<Producer> listProducer) {
        for (Producer pro : listProducer) {
            ProducerInfo producerInfo = new ProducerInfo();
            producerInfo.setSeqNo(pro.getSeqNo());
            producerInfo.setProducerName(pro.getProducerName());
            producerInfo.setSysStatus(pro.getSysStatus());
            //
            RedisCache.saveOrUpdateProducerInfo(producerInfo);
        }
    }

    private String getCsvFileName(String absolutePath) {
        String filePath = absolutePath + File.separator + CSV_EXPORT_FILE_NAME;
        logger.info("File Path on server : " + filePath);
        return filePath;
    }
}
