package spring.boot.batch.tasklet;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import spring.boot.batch.bean.BatchConfiguration;
import spring.boot.batch.config.CommonMemory;
import spring.boot.batch.constant.JasperReportConstant;
import spring.boot.batch.dao.ProductDaoImpl;
import spring.boot.batch.entity.Producer;
import spring.boot.batch.inmem.bean.ProducerRedisCache;
import spring.boot.batch.service.ProductService;

import java.util.List;

@Getter
@Setter
public class ProducerTasklet implements Tasklet, InitializingBean {

    private SettingTasklet settingTasklet;
    private ProductDaoImpl productDao;
    protected ProducerRedisCache producerRedisCache;

    private final Logger logger = LoggerFactory.getLogger(ProducerTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) {
        try {
            logger.info("========== START RUN PRODUCER TASKLET ==========");

            getDateByCommonClass();
            String reportDate = settingTasklet.getReportDate();
            if (reportDate == null) throw new IllegalArgumentException("Report Date is null");
            ProductService productService = getProductService();
            List<Producer> listProducer = productService.getListProducer();

            logger.info("========== START UPDATE DATA FOR REDIS ==========");
            productService.saveProducerOnRedis(listProducer);
            logger.info("========== START LOAD DATA FROM REDIS ==========");
            productService.getListProducerCache();

            productService.exportToFile(listProducer, reportDate, settingTasklet.getDataDailyExportFilePath());
            logger.info("========== FINISH RUN PRODUCER TASKLET ==========");
            CommonMemory.getInstance().updateBatchStatus(JasperReportConstant.SUMMARY_TYPE.UPDATE_PRODUCER_TASKLET, true, null);
        } catch (Exception e) {
            CommonMemory.getInstance().updateBatchStatus(JasperReportConstant.SUMMARY_TYPE.UPDATE_PRODUCER_TASKLET, false, e);
            throw e;
        }

        return RepeatStatus.FINISHED;
    }

    private ProductService getProductService() {
        return new ProductService(productDao, producerRedisCache);
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(settingTasklet.getDataDailyExportFilePath(), "dataDailyExportFilePath must be set");
    }

    private void getDateByCommonClass() {
        //Lấy date từ 1 class common config set vào không thông qua bean SettingTasklet để lấy reportDate
        String reportDate = BatchConfiguration.getBatchConfiguration().getReportDate();
        System.out.println("Report Date from Common class : " + reportDate);
    }

}
