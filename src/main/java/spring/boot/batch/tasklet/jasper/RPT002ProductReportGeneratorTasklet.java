package spring.boot.batch.tasklet.jasper;

import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import spring.boot.batch.config.CommonMemory;
import spring.boot.batch.constant.JasperReportConstant;
import spring.boot.batch.dao.ProductDaoImpl;
import spring.boot.batch.entity.Product;
import spring.boot.batch.tasklet.SettingTasklet;
import spring.boot.batch.tasklet.bean.ReportConfiguration;

import javax.sql.DataSource;
import java.util.List;
import java.util.concurrent.ExecutorService;

@Getter
@Setter
public class RPT002ProductReportGeneratorTasklet implements Tasklet {

    private Logger logger = Logger.getLogger(RPT002ProductReportGeneratorTasklet.class);

    private ProductDaoImpl productDao;
    private SettingTasklet settingTasklet;
    private ReportConfiguration config;
    private DataSource dataSource;

    private ExecutorService generator;

    public static final String REPORT_PREFIX = "RPT015O_DAILY-CUST-REPORT-";

    @Override
    public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
        try {
            logger.info("========== START GENERATION REPORT RPT002 - DAILY PRODUCT TRADING  ==========");

            String reportDate = settingTasklet.getReportDate();
            List<Product> productInfos = productDao.loadAllProduct();
            logger.debug("productInfos loaded: " + productInfos.size());

            if (productInfos.size() > 0) {
                RPT002ProductService rpt002ProductService = getRPT002ProductService();
                logger.debug("Begin export PDF report: RPT_002 for  " + productInfos.size());
                rpt002ProductService.generateReport(productInfos, JasperReportConstant.EXPORT_FILE_TYPE.PDF, reportDate);

                logger.debug("Begin export CSV report: RPT_002 for  " + productInfos.size());
                rpt002ProductService.generateReport(productInfos, JasperReportConstant.EXPORT_FILE_TYPE.CSV, reportDate);
            }

            CommonMemory.getInstance().updateBatchStatus(JasperReportConstant.REPORT_TEMPLATE_CD.M_RPT_PRODUCT, true, null);
            logger.info("========== FINISH GENERATION REPORT RPT002 - DAILY PRODUCT TRADING  ==========");
        } catch (Exception e) {
            CommonMemory.getInstance().updateBatchStatus(JasperReportConstant.REPORT_TEMPLATE_CD.M_RPT_PRODUCT, false, e);
            throw e;
        }
        return RepeatStatus.FINISHED;
    }

    private RPT002ProductService getRPT002ProductService() {
        return new RPT002ProductService(config, dataSource);
    }
}
