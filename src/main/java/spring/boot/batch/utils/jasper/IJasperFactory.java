package spring.boot.batch.utils.jasper;

public interface IJasperFactory {
    boolean exportToPDF();

    boolean exportToCSV();

    boolean exportToTXT();

    boolean exportToXLS();
}
